# CancilleriaGovChallenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

******************************************************************************************
## CHALLENGE EXPOSURE

he decidido hacer esta pagina en angular, teniendo en cuenta que con angular puedo crear un single page application, usando scss como lenguaje de hojas de estilo en cascada.

Empecé con el comando ng new cancilleria-gov-challenge para crear el proyecto en angular.

a continuación instalaré angular material, una librería con componentes muy buenos a los cuales haré algunas modificaciones, mas info en https://material.angular.io/guide/getting-started.

también usaré la librería materializecss, de la cual solo usaré el slider para las imágenes en la página de inicio. todas las imágenes que usaré serán de la web, para evitar la descarga de archivos y por tiempo.

Una vez haga todas las instalaciones solo debo configurar las librerías que usaré, decidí crear un módulo propio para angular  material, en el cual estarán importados los componentes que voy a usar ‘src/app/angular-material/angular-material.module.ts’.

******************************************************************************************
## la estructura de mi proyecto es la siguiente:

## `src/app`
## `...../angular-material`
## `..........angular-material.module`
## `...../interfaces`
## `.....app.component`
## `.....app.module, routing`
## `........../components/`
## `.............../admin`
## `....................admin.module, routing`
## `.............../home`
## `....................home.component`
## `.............../sala-de-prensa`
## `....................sala-de-prensa.component`
## `........................./subcomponentes`
## `............................../comunicados-oficiales`
## `...................................comunicados-oficiales.component`
## `............................../menú-sala-de-prensa`
## `...................................menú-sala-de-prensa.component`
## `.............../vuelos-humanitarios`
## `....................vuelos-humanitarios.component`
## `src/assets`
## `...../scss`
## `..........grids, materialize, scrollbar, theme (estilos de angular material)`
## `src/index, main, styles`

******************************************************************************************
## `Fuente usada: Lexend Deca,`
## `Iconos: Material Icons Round,`
## `Librerías de diseño: `
## `    Materialize css (slider, footer): https://materializecss.com/getting-started.html`
## `    Angular material (`
## `    MatToolbarModule,`
## `        	MatIconModule,`
## `        	MatButtonModule,`
## `        	MatCardModule,`
## `        	MatFormFieldModule,`
## `        	MatInputModule,`
## `        	MatDatepickerModule,`
## `        	MatNativeDateModule,`
## `        	MatMenuModule,): https://material.angular.io/guide/getting-started,`
## `    Bootstrap (grids): https://getbootstrap.com/docs/4.5/getting-started/introduction/`

******************************************************************************************
Para correr el proyecto debe tener instalado NodeJS con npm y Angular(CLI), si estás en linux te recomiendo usar Node Version Manager (NVM).

una vez tengas la instalación correcta, en el directorio del proyecto ejecuta el comando ‘npm install’ o ‘npm i’ para instalar las dependencias.

después de que se instalen ejecutamos el comando ‘ng serve’ la aplicación correrá en el localhost en el puerto 4200 http://localhost:4200/

pd: los elementos del menú con opacidad no tienen rutas.
******************************************************************************************