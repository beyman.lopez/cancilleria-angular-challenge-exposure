import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminModule } from './components/admin/admin.module';

const routes: Routes = [
  {
    // LA URL HOST:PUERTO ME LLEVARÁ A ESTA URL
    path: '',
    redirectTo: 'pagina/inicio',
    pathMatch: 'full',
  }, {
    // UBICACION DEL MODULO HIJO LE ASIGNO LA SIGUIENTE RUTA
    path: 'pagina',
    loadChildren: () => AdminModule,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
