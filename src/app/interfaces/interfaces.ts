// LAS INTERFACES ME AYUDAN A TENER UN TIPADO MAS ESTRICTO EN LA APLICACION

// INTERFAZ DEL MENU
export interface MENU_APP {
    url?: string;
    nombre: string;
    submodulos?: SUBMODULO[]
}
// INTERFAZ DEL MENU
export interface SUBMODULO {
    url?: string;
    nombre: string;
    submodulos?: SUBMODULO[]
}
// INTERFAZ DE LOS COMUNICADOS
export interface COMUNICADOS {
    imagen?: string;
    titulo: string;
    fecha: string;
    contenido: string;
}
// INTERFAX DE LOS ITEMS DEL MENU DE SALA DE PRENSAS
export interface MENU_SALA_PRENSA {
    menu: string;
    items: string[];
}
// INTERFAZ DE LAS NOTICIAS EN SALA DE PRENSAS
export interface NOTICIAS {
    titulo: string;
    contenido: string;
    imagen?: string;
    video?: any;
}