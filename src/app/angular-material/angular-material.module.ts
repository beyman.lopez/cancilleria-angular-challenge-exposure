import { NgModule } from '@angular/core';
// PRIMERO IMPORTO LOS MODULOS QUE VOY A USAR DE ESTA MANERA
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
    // LOS PONGO EN IMPORTS
    imports: [
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMenuModule,
        ///////ANGULAR MATERIAL ///////
        FormsModule,
    ],
    // LOS PONGO EN EXPORTS
    exports: [
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMenuModule,
        ///////ANGULAR MATERIAL ///////
        FormsModule,
    ],
    providers: [
        MatDatepickerModule,
    ],
})
// ESTA ES LA CLASE DE ANGULAR MATERIAL QUE VOY A USAR EN CADA PAGINA
// DEBO IMPORTARLA EN EL MODULO PADRE (APP) Y EN EL HIJO (ADMIN)
export class MaterialModule { }