import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosHumanitariosComponent } from './vuelos-humanitarios.component';

describe('VuelosHumanitariosComponent', () => {
  let component: VuelosHumanitariosComponent;
  let fixture: ComponentFixture<VuelosHumanitariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VuelosHumanitariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosHumanitariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
