import { Component, OnInit } from '@angular/core';
import { MENU_SALA_PRENSA } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-menu-sala-de-prensa',
  templateUrl: './menu-sala-de-prensa.component.html',
  styleUrls: ['./menu-sala-de-prensa.component.scss']
})
export class MenuSalaDePrensaComponent implements OnInit {

  menuSalaPrensa: MENU_SALA_PRENSA[] = [
    {
      menu: 'Sala de Prensa',
      items: [
        'Noticias',
        'Imágenes',
        'Videos',
        'Audios',
        'Comunicados oficiales',
        'Discursos',
        'Especiales Multimedia',
        'Noticias de Embajadas y Consulados',
        'Calendario de actividades',
        'Contacto Prensa',
      ]
    },
    {
      menu: '2020',
      items: [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
      ]
    },
    {
      menu: 'Años Anteriores',
      items: [
        '2020',
        '2019',
        '2018',
        '2017',
      ]
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
