import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSalaDePrensaComponent } from './menu-sala-de-prensa.component';

describe('MenuSalaDePrensaComponent', () => {
  let component: MenuSalaDePrensaComponent;
  let fixture: ComponentFixture<MenuSalaDePrensaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuSalaDePrensaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSalaDePrensaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
