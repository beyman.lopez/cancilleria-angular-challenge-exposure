import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunicadosOficialesComponent } from './comunicados-oficiales.component';

describe('ComunicadosOficialesComponent', () => {
  let component: ComunicadosOficialesComponent;
  let fixture: ComponentFixture<ComunicadosOficialesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunicadosOficialesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComunicadosOficialesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
