import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { COMUNICADOS } from 'src/app/interfaces/interfaces';
@Component({
  selector: 'app-comunicados-oficiales',
  templateUrl: './comunicados-oficiales.component.html',
  styleUrls: ['./comunicados-oficiales.component.scss']
})
export class ComunicadosOficialesComponent implements OnInit {
  // COMUNICADOS INICIALES DE TIPO INTERFAZ COMUNICADOS
  comunicados: COMUNICADOS[] = [
    {
      titulo: 'Comunicado de prensa',
      fecha: '30/09/2020',
      contenido: `La Cancillería, en nombre del Gobierno Nacional, se permite denunciar ante la comunidad
      internacional un nuevo hecho de abuso de autoridad, sanción desproporcionada frente a una
      eventual
      contravención y desconocimiento flagrante a los acuerdos de libre navegabilidad por parte de
      fuerzas
      militares que respaldan al régimen ilegítimo de Nicolás Maduro, en contra de población civil
      colombiana.`
    },
    {
      titulo: 'Comunicado de prensa sobre el Informe de la Misión Internacional Independiente de determinación de los hechos sobre Venezuela',
      fecha: '17/09/2020',
      contenido: `El Ministerio de Relaciones Exteriores, en nombre del Gobierno de Colombia, acoge las conclusiones del Informe de la Misión Internacional Independiente de determinación de los hechos de las Naciones Unidas sobre Venezuela, dado a conocer el 16 de septiembre de 2020 en Ginebra.`
    },
    {
      titulo: 'Colombia, nuevo miembro del Tratado de Amistad y Cooperación (TAC) del ASEAN',
      fecha: '14/09/2020',
      contenido: `El Ministerio de Relaciones Exteriores, en nombre del Gobierno de Colombia, celebra y agradece la decisión adoptada por los Ministros de Relaciones Exteriores de la Asociación de Naciones del Sudeste Asiático -ASEAN - para admitir a Colombia como nuevo miembro del Tratado de Amistad y Cooperación -TAC, tal como fue anunciado por su Presidente pro-tempore y Ministro de Relaciones Exteriores de Vietnam, Pham Binh Minh, el 12 de septiembre.`
    },
    {
      titulo: 'Colombia respaldó y acogió positivamente la elección de Mauricio Claver-Carone como presidente del BID',
      fecha: '12/09/2020',
      contenido: `Bogotá (sep. 12/20). La Asamblea de Gobernadores del BID eligió hoy a Mauricio Claver-Carone como el nuevo presidente del Banco Interamericano de Desarrollo (BID).`
    },
    {
      titulo: 'Comunicado de Prensa',
      fecha: '26/08/2020',
      contenido: `El día 18 de agosto, el Ministerio de Relaciones Exteriores recibió el Oficio MJD-OFI20-0027399-DAI-1100 del 18 de agosto de 2020, del Ministerio de Justicia y del Derecho mediante el cual remite, a su vez, el oficio No 178 del 18 de agosto de 2020 procedente de la Sala de Justicia y Paz del Tribunal Superior del Distrito Judicial de Bogotá, en el que se ordena, con carácter urgente, presentar ante el Gobierno de los Estados Unidos de América la solicitud de captura y posterior extradición del señor Salvatore Mancuso Gómez.`
    },
    {
      titulo: 'Comunicado de Prensa con relación al proceso de extradición del señor Salvatore Mancuso Gómez',
      fecha: '23/08/2020',
      contenido: `El Gobierno Nacional, con relación al proceso de extradición del señor Salvatore Mancuso Gómez, informa y aclara lo siguiente:`
    },
    {
      titulo: 'Comunicado de Prensa',
      fecha: '20/08/2020',
      contenido: `El Ministerio de Relaciones Exteriores, en el ejercicio de sus funciones de canal diplomático en el trámite de extradición, informa lo siguiente en relación con el caso del señor Salvatore Mancuso Gómez:`
    },
    {
      titulo: 'Comunicado de Prensa',
      fecha: '18/08/2020',
      contenido: `El Ministerio de Relaciones Exteriores, en nombre del Gobierno Nacional, toma nota de los trinos publicados en la mañana de hoy por los Embajadores de Alemania y Francia en Colombia y, al respecto, se permite expresar lo siguiente:`
    },
    {
      titulo: 'Comunicado sobre nuevos vuelos de carácter humanitario de la segunda quincena de agosto',
      fecha: '10/08/2020',
      contenido: `La Cancillería, el Ministerio de Transporte y Migración Colombia continúan trabajando de manera articulada en la gestión de vuelos de carácter humanitario, que permitan el retorno al país de aquellos colombianos que por la emergencia generada por el COVID-19 no han podido regresar.`
    },
    {
      titulo: 'Comunicado sobre Guyana',
      fecha: '6/08/2020',
      contenido: `Los gobiernos de Brasil, Colombia, Chile, Ecuador, Paraguay y Perú , países miembros del Foro para el Progreso e Integración de América del Sur (PROSUR) y Bolivia en calidad de Observador, felicitan a Mohamed Irfaan Ali por su elección en la presidencia de la República Cooperativa de Guyana y valoran el trabajo realizado por la Misión de Observación Electoral de la Comunidad del Caribe (CARICOM) y el respaldo de la Organización de Estados Americanos (OEA).`
    },
  ]
  // COMUNICADOS A FILTRAR Y MOSTRAR EN EL DOM CON NGFOR
  comunicadosFiltrados: COMUNICADOS[] = []
  // VALOR DE BUSQUEDA
  buscarValor: string = "";

  constructor() { }

  ngOnInit(): void {
    // SETEO LOS VALORES AL INICIO
    this.comunicadosFiltrados = this.comunicados;
  }

  buscarComunicado() {
    // FILTRO EL ARRAY USANDO LOS TRES VALORES, EL TOLOWERCASE Y EL INCLUDES PARA QUE LA BUSQUEDA SEA MAS PRECISA Y TENGA MAS ALCANCE
    this.comunicadosFiltrados = this.comunicados.filter((comunicado) => {
      return comunicado.titulo.toLowerCase().includes(this.buscarValor.toLowerCase()) ||
        comunicado.fecha.toLowerCase().includes(this.buscarValor.toLowerCase()) ||
        comunicado.contenido.toLowerCase().includes(this.buscarValor.toLowerCase())
    });
  }

}
