import { Component, OnInit } from '@angular/core';
import { COMUNICADOS } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-sala-de-prensa',
  templateUrl: './sala-de-prensa.component.html',
  styleUrls: ['./sala-de-prensa.component.scss']
})
export class SalaDePrensaComponent implements OnInit {

  // COMUNICADOS INICIALES DE TIPO INTERFAZ COMUNICADOS
  comunicados: COMUNICADOS[] = [
    {
      titulo: 'Colombia y Japón llevaron a cabo la reunión de diálogo político',
      fecha: '9/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/japon.png?itok=fMluoJE0',
      contenido: `Bogotá (oct. 8/20). Con el fin de revisar los principales temas de la agenda bilateral, el Director de Asia, África y Oceanía, Alfredo Ramos, participó de manera virtual en la cuarta reunión de diálogo político con el Director General para América Latina y el Caribe de Japón, Teiji Hayashi.`
    },
    {
      titulo: 'Presentación de copias de estilo de cartas credenciales del Embajador de Países Bajos en Colombia ante la Canciller Claudia Blum',
      fecha: '8/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/presentaciondecopiasdeestilodecartascredencialesdelembajadordepaisesbajosencolombiaantelacancillercl.jpeg?itok=iSwGPJ1-',
      contenido: `Bogotá (oct. 8/20) . En ceremonia realizada en el Palacio de San Carlos, el nuevo Embajador del Reino de los Países Bajos en Colombia, Ernst Noorman, presentó copias de estilo de sus cartas credenciales ante la Canciller Claudia Blum.`
    },
    {
      titulo: 'Colombia y Vietnam celebraron la V reunión de consultas políticas',
      fecha: '8/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/whatsappimage2020-10-08at30503pm1.jpeg?itok=rI-wGnVb',
      contenido: `Bogotá (oct. 8/20). El viceministro de Relaciones Exteriores, Francisco Echeverri, lideró la V reunión del mecanismo de consultas políticas entre Colombia y Vietnam, junto con su homólogo vietnamita, el Primer Viceministro de Relaciones Exteriores, Bui Thanh Son.`
    },
    {
      titulo: 'Canciller Blum presidió V Comisión de Asuntos Políticos y Sociales entre Colombia y Chile',
      fecha: '8/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/prensa_noticias/public/newsroom/news/images/vcomisiondeasuntospoliticosysociales-2020.jpeg?itok=O4gnfmhF',
      contenido: `Bogotá (oct. 8/20). Los Ministros de Relaciones Exteriores de Colombia y de Chile, Claudia Blum y Andrés Allamand, se reunieron este jueves en la V Comisión de Asuntos Políticos y Sociales entre ambos países, con el objetivo de debatir y avanzar en asuntos de gran importancia para la agenda bilateral y el fortalecimiento de las relaciones.`
    },
    {
      titulo: '“Colombia y Unión Europea elevarán el nivel de su relación estratégica”: Canciller Claudia Blum al terminar reunión con la Embajadora de la UE Patricia Llombart',
      fecha: '7/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/colombiayunioneuropeaelevaranelniveldesurelacionestrategicacancillerclaudiablumalterminarreunionconl.jpeg?itok=5HEs_D_n',
      contenido: `Bogotá (oct. 7/20). La Canciller Claudia Blum sostuvo un encuentro con la Embajadora de la Unión Europea (UE) en Colombia, Patricia Llombart, en el que se ratificó la alianza estratégica de la UE y Colombia.`
    },
    {
      titulo: 'Proyectos Normativos',
      fecha: '6/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/slide-noticias-proyectos-normativos.jpg?itok=jb-6R6ke',
      contenido: `Bogotá (oct. 6/20). Sus observaciones son importantes en la construcción del Decreto "Por medio del cual se modifica el Decreto 3790 de 2008, en relación con la composición de la Sección de Colombia en la Comisión Interamericana del Atún Tropical, CIAT, y de su Comité Consultivo" Haga clic aquí.`
    },
    {
      titulo: 'Colombia participa en la 5ta Reunión extraordinaria del Consejo Ejecutivo de la Organización Mundial de la Salud (OMS) sobre la respuesta a la COVID-19',
      fecha: '6/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/colombiaparticipoenla5tareunionextraordinariadelconsejoejecutivodelaorganizacionmundialdelasaludomss.jpg?itok=DG8_jI-e',
      contenido: `Bogotá (oct. 5/20). Una delegación liderada por el Viceministro de Salud Pública y Prestación de Servicios, Luis Alexander Moscoso, y conformada por funcionarios del Ministerio de Salud y Protección Social, la Misión Permanente de Colombia en Ginebra y la Cancillería, participa en la 5ta Reunión extraordinaria del Consejo Ejecutivo de la Organización Mundial de la Salud (OMS) sobre la respuesta a la COVID-19, la cual tuvo lugar los días 5 y 6 octubre de forma virtual y presencial.`
    },
    {
      titulo: 'Colombia y Perú celebraron la XII Reunión de la Comisión Mixta de Drogas',
      fecha: '6/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/olombiayperucelebraronlaxiireuniondelacomisionmixtadedrogas.jpg?itok=My5YLgMW',
      contenido: `Bogotá, (sep. 30/20). Colombia y Perú celebraron la XII Reunión de la Comisión Mixta en Materia de Control de Drogas, la cual se llevó a cabo por primera vez de manera virtual bajo la organización del Grupo de Lucha contra las Drogas de la Cancillería colombiana. Este mecanismo especializado busca intercambiar información, experiencias, buenas prácticas y lecciones aprendidas por los dos países en el abordaje del Problema Mundial de las Drogas, en el marco del Acuerdo suscrito entre las partes en 1998.`
    },
    {
      titulo: 'Cronograma de la Maestría en Análisis de problemas políticos, económicos e internacionales contemporáneos. Promoción XLI (2021-2022)',
      fecha: '6/10/2020',
      imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide_lista/public/newsroom/news/images/cronogramamaestriaidadv2.png?itok=10WStcZS',
      contenido: ``
    }
  ]

  // COMUNICADOS A FILTRAR Y MOSTRAR EN EL DOM CON NGFOR
  comunicadosFiltrados: COMUNICADOS[] = []
  // VALOR DE BUSQUEDA
  buscarValor: string = "";

  constructor() { }

  ngOnInit(): void {
    // SETEO LOS VALORES AL INICIO
    this.comunicadosFiltrados = this.comunicados;
  }

  buscarComunicado() {
    // FILTRO EL ARRAY USANDO LOS TRES VALORES, EL TOLOWERCASE Y EL INCLUDES PARA QUE LA BUSQUEDA SEA MAS PRECISA Y TENGA MAS ALCANCE
    this.comunicadosFiltrados = this.comunicados.filter((comunicado) => {
      return comunicado.titulo.toLowerCase().includes(this.buscarValor.toLowerCase()) ||
        comunicado.fecha.toLowerCase().includes(this.buscarValor.toLowerCase()) ||
        comunicado.contenido.toLowerCase().includes(this.buscarValor.toLowerCase())
    });
  }

}
