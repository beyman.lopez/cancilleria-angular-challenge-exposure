import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as M from "materialize-css/dist/js/materialize.js";
import { NOTICIAS } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('itemsMenu', { read: ElementRef }) public itemsMenu: ElementRef<any>;
  @ViewChild('slider') elSlider: ElementRef;

  // IMAGENES ARRAY DE STRINGS
  imagesUrl: string[] = [
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/vcomisiondeasuntospoliticosysociales-2020.jpeg?itok=J8Q6G2QB',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/colombiayunioneuropeaelevaranelniveldesurelacionestrategicacancillerclaudiablumalterminarreunionconl.jpeg?itok=nwBCyxb2',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/cancilleriatrabajaencooperacioninternacionalyenlosambitosmultilateralybilateralparaapoyarlareactivac.jpeg?itok=63SVPtAe',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/cancillerblumylaaltacomisionadadelasnacionesunidasparalosddhhencolombia.jpg?itok=UhcY2gDm',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/whatsappimage2020-10-02at24517pm.jpeg?itok=_fuai5wg',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/whatsappimage2020-10-02at24517pm.jpeg?itok=_fuai5wg',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/canciller_claudia_blum_participo_en_la_v_reunion_ministerial_de_paises_de_renta_media.jpeg?itok=JEWuWTEk',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/whatsappimage2020-10-08at30503pm1.jpeg?itok=1PnG5dGu',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/slide10.jpg?itok=MFjkEnre',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/cronogramamaestriaidadv2.png?itok=rVmXYse6',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/slide13.jpg?itok=elauHQ6S',
    'https://www.cancilleria.gov.co/sites/default/files/styles/news_slide/public/newsroom/news/images/nuevasdisposicionesencolombiaparalasalidayllegadadeviajeroshaciaydesdeelexterior.jpeg?itok=-rZDz5Tp',
  ]
  // NOTICIAS VACIAS, LAS CARGO EN EL constructor PARA EVITAR EL ERROR CON EL IFRAME
  noticias: NOTICIAS[] = []

  constructor(private sanitizer: DomSanitizer) {

    // EL DomSanitizer ME AYUDA A CARGAR EL VIDEO DE MANERA CORRIGIENDO LA SEGURIDAD DE LA URL YA QUE ESTOY USANDO NGFOR         
    this.noticias = [
      {
        titulo: 'Colombia y Perú celebraron la XII Reunión de la Comisión Mixta de Drogas',
        contenido: `Bogotá, (sep. 30/20). Colombia y Perú celebraron la XII Reunión de la Comisión Mixta en Materia de Control
      de
      Drogas, la cual se llevó a cabo por primera vez de manera virtual bajo la organización del Grupo de Lucha
      contra las Drogas de la Cancillería colombiana. Este mecanismo especializado busca intercambiar
      información,
      experiencias, buenas prácticas y lecciones aprendidas por los dos países en el abordaje del Problema
      Mundial
      de las Drogas, en el marco del Acuerdo suscrito entre las partes en 1998.`,
        imagen: 'https://www.cancilleria.gov.co/sites/default/files/styles/290/public/newsroom/news/images/olombiayperucelebraronlaxiireuniondelacomisionmixtadedrogas.jpg?itok=ZfGAqP9H'
      },
      {
        titulo: 'Palabras de la Ministra de Relaciones Exteriores de Colombia.',
        contenido: `Palabras de la Ministra de Relaciones Exteriores de Colombia, Claudia Blum, con ocasión de la
      conmemoracion del Día Internacional para la Eliminac8ión total de las Armas Nucleares`,
        video: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/VSTapO6Rh1E')
      },
      {
        titulo: 'Colombia participa en la 5ta Reunión extraordinaria del Consejo Ejecutivo de la Organización Mundial de la Salud (OMS) sobre la respuesta a la COVID-19',
        contenido: `Bogotá (oct. 5/20). Una delegación liderada por el Viceministro de Salud Pública y Prestación de
      Servicios, Luis Alexander Moscoso, y conformada por funcionarios del Ministerio de Salud y Protección
      Social, la Misión Permanente de Colombia en Ginebra y la Cancillería, participa en la 5ta Reunión
      extraordinaria del Consejo Ejecutivo de la Organización Mundial de la Salud (OMS) sobre la respuesta a la
      COVID-19, la cual tuvo lugar los días 5 y 6 octubre de forma virtual y presencial.`,
      }
    ]
  }

  ngOnInit(): void {
    document.getElementById('scroll').classList.add('home');
    console.clear();
  }


  ngOnDestroy(): void {
    document.getElementById('scroll').classList.remove('home');
  }

  ngAfterViewInit(): void {
    // INICIALIZO EL SLIDER Y LIMPIO LA CONSOLA
    console.clear();
    let instances = M.Slider.init(this.elSlider.nativeElement, { height: 350, indicators: true });
  }

  // SCROLL A LA DERECHA CUANDO HAGO CLICK EN EL RESPECTIVO BOTON
  public scrollRight(): void {
    this.itemsMenu.nativeElement.scrollTo({ left: (this.itemsMenu.nativeElement.scrollLeft + 400), behavior: 'smooth' });
  }

  // SCROLL A LA IZQUIERDA CUANDO HAGO CLICK EN EL RESPECTIVO BOTON
  public scrollLeft(): void {
    this.itemsMenu.nativeElement.scrollTo({ left: (this.itemsMenu.nativeElement.scrollLeft - 400), behavior: 'smooth' });
  }

}
