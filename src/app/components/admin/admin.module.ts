import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminRoutes } from './admin.routing';
import { MaterialModule } from 'src/app/angular-material/angular-material.module';
import { HomeComponent } from '../home/home.component';
import { VuelosHumanitariosComponent } from '../vuelos-humanitarios/vuelos-humanitarios.component';



@NgModule({
  declarations: [
    HomeComponent,
    VuelosHumanitariosComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
    RouterModule.forChild(AdminRoutes),
  ]
})
export class AdminModule { }
