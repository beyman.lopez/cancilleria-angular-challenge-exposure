import { Routes } from '@angular/router';
import { ComunicadosOficialesComponent } from '../sala-de-prensa/subcomponentes/comunicados-oficiales/comunicados-oficiales.component';
import { HomeComponent } from '../home/home.component';
import { VuelosHumanitariosComponent } from '../vuelos-humanitarios/vuelos-humanitarios.component';
import { SalaDePrensaComponent } from '../sala-de-prensa/sala-de-prensa.component';
// RUTAS DEL MODULO HIJO
export const AdminRoutes: Routes = [
    {
        path: '',
    }, {
        // URL QUE QUIERO PARA EL COMPONENTE
        path: 'inicio',
        // IMPORTACION DEL COMPONENTE
        component: HomeComponent,
        // INFO EXTRA QUE LE PUEDO ENVIAR CON EL ROUTING
        data: { title: 'Inicio' }
    }, {
        path: 'vuelos-humanitarios',
        component: VuelosHumanitariosComponent,
        data: { title: 'Vuelos humanitarios' }
    },
    {
        path: 'sala-de-prensa',
        component: SalaDePrensaComponent,
        data: { title: 'Sala de prensa' },
    },
    {
        path: 'sala-de-prensa/#',
        component: SalaDePrensaComponent,
        data: { title: 'Sala de prensa' },
    }, {
        path: 'sala-de-prensa',
        // SUBCOMPONENTE DE SALA DE PRENSA MODULO HIJO
        children: [
            {
                path: 'comunicados-oficiales',
                component: ComunicadosOficialesComponent,
                data: { title: 'Comunicados oficiales' }
            }
        ]
    }

];