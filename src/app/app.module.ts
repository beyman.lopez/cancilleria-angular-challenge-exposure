import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './angular-material/angular-material.module';
import { ComunicadosOficialesComponent } from './components/sala-de-prensa/subcomponentes/comunicados-oficiales/comunicados-oficiales.component';
import { SalaDePrensaComponent } from './components/sala-de-prensa/sala-de-prensa.component';
import { MenuSalaDePrensaComponent } from './components/sala-de-prensa/subcomponentes/menu-sala-de-prensa/menu-sala-de-prensa.component';

@NgModule({
  // COMPONENTES
  declarations: [
    AppComponent,
    ComunicadosOficialesComponent,
    SalaDePrensaComponent,
    MenuSalaDePrensaComponent,
  ],
  // MODULOS
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // ANGULAR MATERIAL
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }