import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MENU_APP } from './interfaces/interfaces';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cancilleria-gov-challenge';
  // HAGO REFERENCIA AL DIV QUE CONTIENE LOS ITEMS DEL MENU PARA HACER SCROLL CON LOS BOTONES
  @ViewChild('itemsMenu', { read: ElementRef }) public itemsMenu: ElementRef<any>;
  // ITEMS DEL MENU CON SUBMENUS
  menuItems: MENU_APP[] = [
    { url: 'inicio', nombre: 'Inicio' },
    { nombre: 'El Ministerio' },
    { nombre: 'La Ministra' },
    { url: 'vuelos-humanitarios', nombre: 'Vuelos Humanitarios' },
    {
      url: 'sala-de-prensa', nombre: 'Sala de Prensa',
      submodulos: [
        { url: '#', nombre: 'Sala de Prensa' },
        { nombre: 'Noticias' },
        { nombre: 'Noticias' },
        { nombre: 'Contacto Prensa' },
        { nombre: 'Calendario de actividades' },
        { nombre: 'Noticias de Embajadas y Consulados' },
        { nombre: 'Especiales Multimedia' },
        { nombre: 'Discursos' },
        { url: 'comunicados-oficiales', nombre: 'Comunicados oficiales' },
        { nombre: 'Audios' },
        { nombre: 'Vídeos' },
        { nombre: 'Imágenes' },
      ]
    },
    { nombre: 'Política Exterior' },
    { nombre: 'Nuestro País' },
    { nombre: 'Trámites y Servicios' },
    {
      nombre: 'Atención al Ciudadano',
      submodulos: [
        { nombre: 'Canales de Atención' },
        { nombre: 'Transparencia y acceso a información pública' },
        { nombre: 'Proyectos Normativos' },
        { nombre: 'Notificaciones y citaciones' },
        { nombre: 'Factura electrónica' },
        { nombre: 'Notificaciones Judiciales' },
        { nombre: 'Glosario' },
        { nombre: 'Rendición de Cuentas' },
        {
          nombre: 'Preguntas frecuentes sobre trámites y servicios',
          submodulos: [
            { nombre: 'Apostilla y Legalización', },
            { nombre: 'Consulados', },
            { nombre: 'Cooperación Judicial', },
            { nombre: 'Nacionalidad', },
            { nombre: 'Visas', },
            { nombre: 'Pasaportes', },
          ]
        },
        { nombre: 'Convocatorias de empleo' },
        { nombre: 'Participación Ciudadana' },
        { nombre: 'Programas de participación ciudadana ‘Jóvenes a la Cancillería’ y ‘Canciller en la Academia’' },
        { nombre: 'Esquema Atención Ciudadana' },
        { nombre: 'Directorio institucional' },
        { nombre: 'Felicitaciones, Peticiones, Sugerencias, Reclamos, Quejas y Denuncias-PQRSDF' },
      ]
    },
  ]
  constructor(private router: Router) { }

  ngOnInit() {
    // AL CAMBIAR DE RUTA HARÉ SCROLL HACIA ARRIBA
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      document.getElementById("scroll").scrollTo(0, 0)
      console.clear();
    });
    // CUANDO LA POSICION DEL SCROLL ES MAYOR A 10PX HARÉ CAMBIOS EN EL DISEÑO DEL NAVBAR
    document.getElementById('scroll').onscroll = (e: any) => {
      let position = e.target.scrollTop;
      if (position > 10) {
        document.getElementById("scroll").classList.add("no-shadow")
      } else {
        document.getElementById("scroll").classList.remove("no-shadow")
      }
    };
  }

  // SCROLL A LA DERECHA CUANDO HAGO CLICK EN EL RESPECTIVO BOTON
  public scrollRight(): void {
    this.itemsMenu.nativeElement.scrollTo({ left: (this.itemsMenu.nativeElement.scrollLeft + 400), behavior: 'smooth' });
  }

  // SCROLL A LA IZQUIERDA CUANDO HAGO CLICK EN EL RESPECTIVO BOTON
  public scrollLeft(): void {
    this.itemsMenu.nativeElement.scrollTo({ left: (this.itemsMenu.nativeElement.scrollLeft - 400), behavior: 'smooth' });
  }

  // ABRO EL SUBMENU CUANDO PASO EL MOUSE ENCIMA DE UN ITEM QUE TENGA SUBMENU
  openMenu(trigger: MatMenuTrigger) {
    trigger.openMenu();
  }
}
